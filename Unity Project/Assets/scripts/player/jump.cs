﻿/* Game: Let me Off
 * Team: Mongoose
 * Script: jump
 * Programmer: Alexander Ong
 */

using UnityEngine;
using System.Collections;

public class jump : MonoBehaviour {
	
	public static bool ground_check;
	public float jumpForce = 1500f;
	float jumpForceMultiplier;
	bool jump_check;
	
	void OnTriggerEnter2D(Collider2D other) {
		ground_check = true;
	}

	void OnTriggerStay2D(Collider2D other) {
		ground_check = true;
	}
	
	
	void OnTriggerExit2D(Collider2D edge) {
		ground_check = false;
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (ground_check == true && Input.GetButton("Jump") == true && jump_check == false) {
			rigidbody2D.AddForce(Vector2.up * jumpForce);
			jump_check = true;
		}

		// Jump has to be released to jump again
		// Prevents instant jump upon landing
		if (Input.GetButton ("Jump") == false) {
			jump_check = false;
		} else {
			jump_check = true;
		}

		// Height control
		if (ground_check == false && Input.GetButton ("Jump") == false && rigidbody2D.velocity.y > 0) {
			rigidbody2D.AddForce(Vector2.up * -1 * (rigidbody2D.velocity.y * 10));
		}
	}
}
	
	

