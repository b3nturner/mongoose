﻿/* Game: Let me Off
 * Team: Mongoose
 * Script: sprint
 * Programmer: Alexander Ong
 */


using UnityEngine;
using System.Collections;

public class sprint : MonoBehaviour {
	public static float speed_multiplier = 2.0f;
	public static bool sprint_check = false;			// if sprint has been executed

	public sprint() {
		}

	public static void sprint_active() {

		if (Input.GetButton("Sprint") == true && jump.ground_check == true){
			// presses sprint while on ground
			movement.max_speed *= speed_multiplier;
			sprint_check = true;						// user has sprinted

		} else if (Input.GetButton("Sprint") == true && jump.ground_check == false){
			// pressing or holding sprint in air
			if (sprint_check == true) {
				// was sprinting before, continue to sprint
				movement.max_speed *= speed_multiplier;
			}
				// was not sprinting before, default speed
		} else if (Input.GetButton("Sprint") == false && jump.ground_check == true){
			// release sprint and on ground
			// not sprinting speed
			sprint_check = false;  	
			// no speed change; default sprint due to movement script updating and recalculating speed
		} else if (Input.GetButton("Sprint") == false && jump.ground_check == false) {
			sprint_check = false;  	
			/*
			// released/releasing sprint and still in air
			if (sprint_check == true) {
				// was sprinting before
				// continue sprinting
				movement.max_speed *= speed_multiplier;
			}
			*/
		} 
//		Debug.Log (movement.max_speed);
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
}
