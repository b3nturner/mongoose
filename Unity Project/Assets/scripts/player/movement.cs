﻿/* Game: Let me Off
 * Team: Mongoose
 * Script: movement
 * Programmer: Alexander Ong
 */


using UnityEngine;
using System.Collections;

public class movement : MonoBehaviour {

	//adjustable variables
	public float accel = 1.0f;
	public float decel = 3.0f;
	public float speed_limit = 20.0f;

	public static float h_movement;
	public static float max_speed;

	//initializing, no need to change
	float speed = 0.0f;

//	public float HorizontalMovement;
	Animator anim;
	bool facingRight = true;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		h_movement =  Input.GetAxis("Horizontal");
		max_speed = (h_movement) * speed_limit;

		sprint.sprint_active ();																		// changes max_speed

		// Input for right arrow key
		 if (h_movement > 0) {
			if (speed < max_speed - decel / 2) {
				if (speed < 0){   																		// if already moving left
					speed += decel;
				} else {
					speed += accel;
				}
			} else if (speed > max_speed + decel / 2){ 													// slows down for sprint
				speed -= decel;
			}
			transform.position += transform.right * speed * Time.deltaTime;

		// Input for left arrow key
		} else if (h_movement < 0) {
			if (speed > max_speed + decel / 2) {  
				if (speed > 0) {																		// if already moving right
					speed -= decel;
				} else {
					speed -= accel;
				}
			} else if (speed < max_speed - decel / 2) {												// slows down for sprint
				speed += decel;
			}
			transform.position += (transform.right * speed * Time.deltaTime);

		// When there is no input
		} else {
			if (speed > decel) {																		// if moving right
				transform.position += transform.right * speed * Time.deltaTime;
				speed -= decel;
			} else if (speed < -1 * decel) {															// if moving left
				transform.position += transform.right * speed * Time.deltaTime;
				speed += decel;
			}
		}

//		Debug.Log (max_speed);

		// Animation flip
		if (h_movement > 0 && !facingRight) {
			FlipFacing ();
		} else if (h_movement < 0 && facingRight) {
			FlipFacing ();
		}

		// Animator
		GetComponent<Animator> ().SetFloat ("HorizontalMovement", speed);

	}

	public void SpeedDown(float speed){
		accel =2;
		speed_limit =10;
	}
	
	public void SpeedUp(float speed2){
		accel =20;
		speed_limit =100;
	}
	
	public void Reset(int i){
		accel = 5;
		speed_limit = 20;
	}

	void FlipFacing()
	{
		facingRight = !facingRight;
		Vector3 charScale = transform.localScale;
		charScale.x *= -1;
		transform.localScale = charScale;
	}

}
