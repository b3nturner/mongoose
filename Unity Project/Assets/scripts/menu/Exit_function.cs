﻿/* Game: Let me Off
 * Team: Mongoose
 * Script: movement
 * Programmer: Alexander Ong
 */

using UnityEngine;
using System.Collections;
public class Exit_function : MonoBehaviour 
{
	//	public Font font; 
	public Texture2D texture = null;
	//	GUIStyle customButton = new GUIStyle();
	
	public void OnGUI() {
		// creates a button
		if (GUI.Button (new Rect (Screen.width / 2 - Screen.width / 7, Screen.height / 2 + Screen.height / 10, 100, 50), texture)) {
			// Quit on button click
			Application.Quit();
		}
		
		
	}
}