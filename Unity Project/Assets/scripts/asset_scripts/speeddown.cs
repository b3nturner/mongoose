﻿using UnityEngine;
using System.Collections;

public class speeddown: MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D c){
		
		c.GetComponent<movement> ().SpeedDown (50.0f);
	}
	
	
	void OnTriggerExit2D(Collider2D c){
		
		c.GetComponent<movement>().Reset (1);
	}
}
